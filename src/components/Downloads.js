import React from "react";
import DownloadElement from "./DownloadElement";

const fetch = window.fetch;
const WebSocket = window.WebSocket;

export default class Downloads extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            torrents: [],
            addUri: ""
        }
        this.ws = null;

        this.magnetChanged = this.magnetChanged.bind(this);
        this.setState = this.setState.bind(this);
        this.postTorrent = this.postTorrent.bind(this);
        this.socketCreate = this.socketCreate.bind(this);
    }


    async componentDidMount() {
        this.socketCreate();
    }

    // Closes the WebSocket when navigating away from the component
    async componentWillUnmount() {
        this.ws.close();
    }

    async getTorrents() {
        return fetch("http://localhost:1345/torrents")
            .then(response => response.json());
    }

    async postTorrent() {
        return fetch('http://localhost:1345/torrents/new', {
            headers: new Headers({'Content-Type': 'application/json'}),
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                'postData': {
                    'magnetUri': this.state.addUri
                }
            })
        });
    }

    async socketCreate() {
        this.ws = new WebSocket('ws://localhost:1345/torrents');

        this.ws.onmessage = (event) => {
            const msg = JSON.parse(event.data);
            const infoHash = msg.infoHash;
            console.log(event.data);

            let newTorrents = []
            let found = false;
            for (let i = 0; i < this.state.torrents.length; i++) {
                if (this.state.torrents[i].infoHash == infoHash) {
                    newTorrents.push(msg);
                    found = true;
                } else {
                    newTorrents.push(this.state.torrents[i]);
                }
            }
            if (!found) {
                newTorrents.push(msg);
            }

            this.setState({
                torrents: newTorrents
            })
        };
    }

    magnetChanged(event) {
        this.setState({
            addUri: event.target.value
        });
    }

    render() {
        const torrentElems = this.state.torrents.map(torrent => <DownloadElement key={torrent.infoHash}
                                                                                 torrent={torrent}/>);

        return (
            <div>

                <form>
                    <div className="form-group">
                        <label htmlFor="magnet-input"> Magnet input </label>
                        <input id="magnet-input" type="text" onChange={this.magnetChanged} className="form-control"/>
                    </div>
                    <div className="form-group">
                        <button type="button" className="btn btn-sucess" onClick={this.postTorrent}> Add</button>
                    </div>
                </form>

                { torrentElems }

            </div>
        );
    }
}

