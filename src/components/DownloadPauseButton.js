import React from "react";

export default class DownloadPauseButton extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const pauseText = this.props.paused ? <i className='material-icons'>play_arrow</i> :
            <i className='material-icons'>pause</i>

        return (
            <div className="col-2 d-flex align-items-center flex-row-reverse">
                <button className="btn btn-outline-primary btn-sm" onClick={this.props.handler}> {pauseText} </button>
            </div>
        );
    }
}

