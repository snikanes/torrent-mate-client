import React, {Component} from "react";
import {Link} from "react-router-dom";

class Navbar extends Component {

    render() {
        return (
            <nav className="navbar navbar-toggleable-md navbar-light transparent-bg">
                <button className="navbar-toggler navbar-toggler-right remove-outline" type="button"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <Link className=" navbar-brand nav-link" to="/">
                    TorrentBot
                </Link>
            </nav>
        );
    }
}

export default Navbar
