import React from "react";

export default class ProgressBar extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const progressPercent = (this.props.progress * 100).toFixed(2);
        const progressStyle = {
            width: `${progressPercent}%`
        }
        const progressStyles = this.props.paused ? "progress-bar progress-bar-striped progress-paused" : "progress-bar progress-bar-striped progress-bar-animated"

        return (
            <div className="progress col-10 d-flex align-items-center">
                <div className={progressStyles} role="progressbar" style={progressStyle} aria-valuenow={progressPercent}
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        );
    }
}

