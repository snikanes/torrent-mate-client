import React, {Component} from "react";
import {BrowserRouter, Route} from "react-router-dom";

import Navbar from "./Navbar";
import Downloads from "./Downloads";

class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <div className="route-root">
                    <Navbar />
                    <div className="container">
                        <Route exact path="/" component={Downloads}/>
                    </div>
                </div>
            </BrowserRouter>
        )
    }
}

export default App
