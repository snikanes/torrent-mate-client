import React from "react";
import ProgressBar from "./ProgressBar";
import DownloadPauseButton from "./DownloadPauseButton";

export default class DownloadElement extends React.Component {

    constructor(props) {
        super(props);

        this.pauseTorrent = this.pauseTorrent.bind(this);
        this.resumeTorrent = this.resumeTorrent.bind(this);
    }

    async pauseTorrent() {
        return fetch('http://localhost:1345/torrents/pause', {
            headers: new Headers({'Content-Type': 'application/json'}),
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                'postData': {
                    'infoHash': this.props.torrent.infoHash
                }
            })
        });
    }

    async resumeTorrent() {
        return fetch('http://localhost:1345/torrents/resume', {
            headers: new Headers({'Content-Type': 'application/json'}),
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                'postData': {
                    'infoHash': this.props.torrent.infoHash
                }
            })
        });
    }

    render() {


        const pauseHandler = this.props.torrent.paused ? this.resumeTorrent : this.pauseTorrent

        return (
            <div className="card">
                <div className="card-block">
                    <h4 className="card-title">{ this.props.torrent.name } </h4>
                    <h6 className="card-subtitle mb-2 text-muted">{`${(this.props.torrent.progress * 100).toFixed(2)}%`}</h6>

                    <div className="row">
                        <ProgressBar paused={this.props.torrent.paused} progress={this.props.torrent.progress}/>
                        <DownloadPauseButton paused={this.props.torrent.paused} handler={pauseHandler}/>
                    </div>
                </div>
            </div>
        );
    }

    // <p> Length: { this.props.torrent.length } </p>
    // 			<p> Ready: { this.props.torrent.ready } </p>
    // 			<p> Destroyed: { this.props.torrent.destroyed } </p>
    // 			<p> Done: { this.props.torrent.done } </p>


}